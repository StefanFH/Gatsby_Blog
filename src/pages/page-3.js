import React from 'react'
import Link from 'gatsby-link'

const ThirdPage = () => (
  <div>
    <h1>Webprojekt Angular 6, Angular Material, Firebase</h1>
    <p>
        Wenn du interesse an einer Webanwendung hast mit der du Personen verwalten kannst dann Besuch doch einmal <a href="https://angulartryout.firebaseapp.com">PersonCrud</a> .
    </p>
    <p>
        Verwendete Technologien:
        <ul>
        <li>Typescript</li>
        <li>Angular 6</li>
        <li>Angular Material</li>
        <li>Firebase Hosting</li>
        <li>Firebase Realtime Database</li>
      </ul>
    </p>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default ThirdPage
