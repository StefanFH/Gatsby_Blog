import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <div>
      <h1>Hallo an alle!</h1>
      <h3>Ich möchte euch auf meinem Blog begrüßen!</h3>
      <p>
        Der Blog wurde mithilfe von Gatsby erstellt.
        Siehe GitHub für aktuelle <a href="https://github.com/gatsbyjs/gatsby">Gatsby</a> entwicklungen.
      </p>
    </div>
    <div>
      <p>
      <Link to="/page-2/">Verwendete Technologien von Gatsby</Link>
      </p>
      <p>
        <Link to="/page-3/">Webprojekt Angular 6, Angular Material, Firebase</Link>
      </p>
    </div>
  </div>
)

export default IndexPage
