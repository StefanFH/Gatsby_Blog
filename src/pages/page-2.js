import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <h1>Verwendete Technologien von Gatsby</h1>
    <p>
      Verwendete Technologien in Gatsby
      <ul>
        <li>React</li>
        <li>GraphQL</li>
        <li>HTML</li>
        <li>CSS</li>
        <li>und noch viel mehr</li>
      </ul>
    </p>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

export default SecondPage
